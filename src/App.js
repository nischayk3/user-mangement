import React from 'react';
import './App.css';
import { Router, Route, Link, Switch, Redirect } from 'react-router-dom';
import Login from './Login/Login'
import Registration from './Registration/Registration'
import Home from './Home/Home'
import { history } from './_helpers';


import { PrivateRoute } from './_components';
function App() {
  return (
    <div className="App">
      <Router history={history}>
        <ul>
          <li> <Link to="/">Home</Link> </li>
          <li>  <Link to="/Login">Login</Link> </li>
          <li><Link to="/Registration">Registration</Link> </li>
        </ul>
        <Switch>
          <PrivateRoute exact path="/" component={Home} />
          {/* <Route exact path='/' component={Home}></Route>  */}
          <Route exact path='/Login' component={Login}></Route>
          <Route exact path='/Registration' component={Registration}></Route>
          <Redirect from="*" to="/" />
        </Switch>

      </Router>
    </div>
  );
}

export default App;
