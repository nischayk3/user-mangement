import React,{useState,useEffect }  from 'react'
import { useLocation } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Link } from 'react-router-dom';

import { userActions } from '../Action';

function Login() {
    const [inputs, setInputs] = useState({
        username: '',
        password: ''
    });
    
    const { username, password } = inputs;
    const dispatch = useDispatch();
    const location = useLocation();
    function handleChange(e) {
        const { name, value } = e.target;
        setInputs(inputs => ({ ...inputs, [name]: value }));
    }

    useEffect(() => { 
        dispatch(userActions.logout()); 
    }, [dispatch]);


    function handleSubmit(e) {
        e.preventDefault();
        console.log("hello")
        console.log(username, password)
        console.log(location)
        
        if (username && password) {
            // get return url from location state or default to home page
            const { from } = location.state || { from: { pathname: "/" } };
            console.log(from)

            dispatch(userActions.login(username, password, from));
        }
    }
    return (
        <div className="col-lg-8 offset-lg-2">
            <h2>Login</h2>
            <form name="form" onSubmit={handleSubmit}>
                <div className="form-group">
                    <label>Username</label>
                    <input type="text" name="username" value={username} onChange={handleChange}  />
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="password" name="password" value={password} onChange={handleChange}  />
                    
                </div>
                <div className="form-group">
                    <button className="btn btn-primary" type ='submit'>
                        {/* {loggingIn && <span className="spinner-border spinner-border-sm mr-1"></span>} */}
                        Login
                    </button>

                    <Link to="/Register" className="btn btn-link">Register</Link>
                    
                </div>
            </form>
        </div>
    )
}

export default Login
