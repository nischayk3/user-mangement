import React, { useState,useEffect } from 'react';
import { useDispatch } from 'react-redux';
import  './register.css'
import { userActions } from '../Action';
import { Link } from 'react-router-dom';


function Registration() {
    const [user, setUser] = useState({
        firstName: '',
        lastName: '',
        username: '',
        password: ''
    });
    
    const dispatch = useDispatch();
    useEffect(() => {
        dispatch(userActions.logout());
    }, [dispatch]);


    function handleChange(e) {
        const { name, value } = e.target;
        setUser(user => ({ ...user, [name]: value }));
    }

    function handleSubmit(e) {
        e.preventDefault();
        console.log("hello")
        console.log(user)
        if (user.firstName && user.lastName && user.username && user.password) {
            dispatch(userActions.register(user));
        }
    }

    return (
        <div className="col-lg-8 offset-lg-2">
            <h2>Register</h2>
            <form name="form" onSubmit={handleSubmit} >
                <div className="form-group">
                    <label>First Name</label>
                    <input type="text" name="firstName" value={user.firstName} onChange={handleChange}  />
                    
                </div>
                <div className="form-group">
                    <label>Last Name</label>
                    <input type="text" name="lastName" value={user.lastName} onChange={handleChange}  />
                </div>
                <div className="form-group">
                    <label>Username</label>
                    <input type="text" name="username" value={user.username} onChange={handleChange}  /> 
                </div>
                <div className="form-group">
                    <label>Password</label>
                    <input type="password" name="password" value={user.password} onChange={handleChange} />
                   
                </div>
                <div className="form-group">
                <button className="btn btn-primary" type ='submit'>
                        {/* {registering && <span className="spinner-border spinner-border-sm mr-1"></span>} */}
                        Register
                    </button>
                    <Link to="/Login" className="btn btn-link">Cancel</Link>
                </div>
            </form>
        </div>
    )
}

export default Registration
